import numpy as np
from sklearn.metrics import roc_auc_score
from FeatureVectorGen import PrintMetrics;
def evaluateSVMLight(validationPath, scorePath):
    length = 0;
    with open(validationPath) as validationFile:
        validationFile.next();
        for line in validationFile:
            if (len(line.strip()) != 0):
                length += 1;
    labels = np.zeros((length, ))
    with open(validationPath) as validationFile:
        validationFile.next();
        ind = 0;
        for line in validationFile:
            words = line.strip().split(" ");
            label = float(words[0]);
            labels[ind] = (label + 1) / 2;
            ind += 1;
    scores = np.zeros((length, ))
    with open(scorePath) as scoreFile:
        ind = 0;
        for line in scoreFile:
            score = float(line.strip());
            scores[ind] = score;
            ind += 1;
    auc = roc_auc_score(labels, scores)
    return auc

def evaluateSVMLightRoc(validationPath, scorePath):
    length = 0;
    with open(validationPath) as validationFile:
        validationFile.next();
        for line in validationFile:
            if (len(line.strip()) != 0):
                length += 1;
    labels = np.zeros((length, ))
    with open(validationPath) as validationFile:
        validationFile.next();
        ind = 0;
        for line in validationFile:
            words = line.strip().split(" ");
            label = float(words[0]);
            labels[ind] = (label + 1) / 2;
            ind += 1;
    scores = np.zeros((length, ))
    with open(scorePath) as scoreFile:
        ind = 0;
        for line in scoreFile:
            score = float(line.strip());
            scores[ind] = score;
            ind += 1;
    from sklearn.metrics import roc_curve
    roc = roc_curve(labels, scores)
    return roc

def sign(x):
    if (x > 0):
        return 1.0;
    else:
        return 0.0;

from poster import calcMetrics

def evaluateSVMAll(validationPath, scorePath):
    length = 0;
    with open(validationPath) as validationFile:
        validationFile.next();
        for line in validationFile:
            if (len(line.strip()) != 0):
                length += 1;
    labels = np.zeros((length, ));
    with open(validationPath) as validationFile:
        validationFile.next();
        ind = 0;
        for line in validationFile:
            words = line.strip().split(" ");
            label = float(words[0]);
            labels[ind] = (label + 1) / 2;
            ind += 1;
    scores = np.zeros((length, ));
    predictions = np.zeros((length, ));
    with open(scorePath) as scoreFile:
        ind = 0;
        for line in scoreFile:
            score = float(line.strip());
            scores[ind] = score;
            predictions[ind] = sign(score);
            ind += 1;
    return calcMetrics(labels, predictions, scores)

# evaluateSVMLight('../data/svmlight/validation/validation-svmlight.dat', '../data/svmlight/model/validation.out')
