input_file_name = 'FeatureVectorWithLabel.csv';
v3_wl_file = '../data/report/v3.features';
train_input_file = '../data/train/' + input_file_name;
validation_input_file = '../data/validation/' + input_file_name;
test_input_file = '../data/test/' + input_file_name;
rootDir = '../data/svmlight/v3'
train_out_file = '%s/train/train-svmlight.dat' % rootDir;
validation_file = '%s/validation/validation-svmlight.dat' % rootDir;

import os

from evaluatesvmlight import evaluateSVMLight

params = [1.0E-1, 1.0E-2, 1.0E-3, 1.0E-4, 1.0E-5, 1.0E-6];

print('Gaussian');
os.system("mkdir -p %s/model/gaussian" % rootDir)
gammas = [1.0E-3, 1.0E-2, 1.0E-1, 1.0, 1.0E1, 1.0E2, 1.0E3];
with open("%s/metrics/gaussian.metrics" % rootDir, "w") as out:
    for g in gammas:
        for lam in params:
            arg = '-c %f -t 2 -m 5000 -g %f' % (lam, g);
            id = arg.replace("-", "").replace(" ", "_");
            model_file = '%s/model/gaussian/%s' % (rootDir, id);
            prediction_file = '%s/model/gaussian/prediction-%s' % (rootDir, id);
            os.system("../svmlight/svm_learn %s %s %s" % (arg, train_out_file, model_file))
            os.system("../svmlight/svm_classify %s %s %s" % (validation_file, model_file, prediction_file))
            auc = evaluateSVMLight(validation_file, prediction_file)
            out.write("%f:%s" % (auc, arg))
