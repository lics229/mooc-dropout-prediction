__author__ = 'dpeng'

from poster import train_feature_matrix, train_target_vector, trainAdaBoost, trainGB, trainLR, trainRF, test_feature_matrix, test_target_vector
from evaluatesvmlight import evaluateSVMAll

def main():
    models = [trainGB, trainRF, trainAdaBoost, trainLR];
    # models = [];
    f = open('../data/final/gaussian.metrics', 'w');
    for model in models:
        (auc, f10, f11, _, mat) = model(train_feature_matrix, train_target_vector, test_feature_matrix, test_target_vector);
        f.write(str(type(model)) + "\n");
        f.write("AUC: %f\n" % auc);
        f.write("F1 (neg): %f\n" % f10);
        f.write("F1 (pos): %f\n" % f11);
        f.write("Confusion matrix: %s\n" % str(mat));
        f.write("-------------------\n");
    (auc, f10, f11, _, mat) = evaluateSVMAll('/home/dpeng/study/cs229/mooc-dropout-prediction/data/final/svmlight/test_svm_light.dat',
                   '/home/dpeng/study/cs229/mooc-dropout-prediction/data/final/svmlight/linear/c_0.001000_m_10000_q_50_n_25_v_3/test-predictions.out');
    f.write("Linear SVM\n");
    f.write("AUC: %f\n" % auc);
    f.write("F1 (neg): %f\n" % f10);
    f.write("F1 (pos): %f\n" % f11);
    f.write("Confusion matrix: %s\n" % str(mat));
    f.write("-------------------\n");
    (auc, f10, f11, _, mat) = evaluateSVMAll('/home/dpeng/study/cs229/mooc-dropout-prediction/data/final/svmlight/test_svm_light.dat',
                                             '/home/dpeng/study/cs229/mooc-dropout-prediction/data/final/svmlight/gaussian/c_0.100000_m_10000_t_2_g_0.001000_v_3/test-predictions.out');
    f.write("Gaussian SVM\n");
    f.write("AUC: %f\n" % auc);
    f.write("F1 (neg): %f\n" % f10);
    f.write("F1 (pos): %f\n" % f11);
    f.write("Confusion matrix: %s\n" % str(mat));
    f.write("-------------------\n");
    f.close();
    print("AUC: %f" % auc)

if __name__ == '__main__':
    main();
