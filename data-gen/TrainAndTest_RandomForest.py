__author__ = 'dpeng'

from FeatureVectorGen import readFeatureFromCache

train_target_vector, train_feature_matrix, features = readFeatureFromCache('../data/train/FeatureVectorWithLabel.csv');
validation_target_vector, validation_feature_matrix, features = readFeatureFromCache(
    '../data/validation/FeatureVectorWithLabel.csv');

from sklearn.ensemble import RandomForestClassifier


num_trees = [10, 20, 50, 80, 100, 150, 200, 300, 400, 500];
criteria = ['gini', 'entropy'];
max_feats = range(2, 10);
print("Random forest")
from FeatureVectorGen import PrintMetrics
for nt in num_trees:
    for ct in criteria:
        for nf in max_feats:
            print("Param: num_tree %d, criterion: %s, max_features %d" % (nt, ct, nf))
            rf = RandomForestClassifier(n_estimators=nt, criterion=ct, max_features=nf)
            rf.fit(train_feature_matrix, train_target_vector);
            predicted = rf.predict(validation_feature_matrix);
            PrintMetrics(validation_target_vector, rf.predict(validation_feature_matrix), rf.predict_proba(
                validation_feature_matrix)[:, 1]);


