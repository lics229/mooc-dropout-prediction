__author__ = 'dpeng'
def cleanData(input_file, whitelist_file, output_file):
    feature_whitelist = [];
    with open(whitelist_file) as wl_file:
        for line in wl_file:
            if (len(line.strip()) != 0):
                feature_whitelist.append(line.strip());
    feature_whitelist_set = set(feature_whitelist);
    with open(input_file) as open_input_file, open(output_file, 'w') as open_output_file:
        header = open_input_file.next().strip();
        words = header.strip().split(',');
        whitelist_indexes = [index for index, word in enumerate(words) if word in feature_whitelist_set];
        features = ','.join([word for word in words if word in feature_whitelist_set]);
        open_output_file.write('enrollment_id,label,%s\n' % features);
        for line in open_input_file:
            words = line.strip().split(',');
            features = ','.join([words[i] for i in whitelist_indexes]);
            open_output_file.write('%s,%s,%s\n' % (words[0], words[1], features))
#
# cleanData('../data/train/FeatureVectorWithLabel.csv', '../data/poster/whitelist',
#           '../data/poster/train/FeatureVectorWithLabel.csv')
# cleanData('../data/validation/FeatureVectorWithLabel.csv', '../data/poster/whitelist',
#           '../data/poster/validation/FeatureVectorWithLabel.csv')
# cleanData('../data/test/FeatureVectorWithLabel.csv', '../data/poster/whitelist',
#           '../data/poster/test/FeatureVectorWithLabel.csv')
train_source = '../data/train/FeatureVectorWithLabel.csv';
validation_source = '../data/validation/FeatureVectorWithLabel.csv';
test_source = '../data/test/FeatureVectorWithLabel.csv';
output_root = '../data/final';
versions = ['v1', 'v2', 'v3', 'v4', 'v5'];
file_name = 'FeatureVectorWithLabel.csv';
from os import makedirs
from os.path import isdir
for v in versions:
    train_dir = '%s/%s/train' % (output_root, v);
    validation_dir = '%s/%s/validation' % (output_root, v);
    test_dir = '%s/%s/test' % (output_root, v);
    if (not isdir(train_dir)):
        makedirs(train_dir);
    if (not isdir(validation_dir)):
        makedirs(validation_dir);
    if (not isdir(test_dir)):
        makedirs(test_dir);
    whitelist_file = '%s/%s/whitelist' % (output_root, v)
    cleanData(train_source, whitelist_file, '%s/%s' % (train_dir, file_name))
    cleanData(validation_source, whitelist_file, '%s/%s' % (validation_dir, file_name))
    cleanData(test_source, whitelist_file, '%s/%s' % (test_dir, file_name))
