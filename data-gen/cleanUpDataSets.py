import shutil

root_dir = '../data/complete/'
train_dir = root_dir + '../train/';
test_dir = root_dir + '../test/';
validation_dir = root_dir + '../validation/';


shutil.rmtree(train_dir);
shutil.rmtree(test_dir);
shutil.rmtree(validation_dir);
