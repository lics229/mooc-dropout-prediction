#!/bin/bash

mkdir -p ../data/svmlight/train
mkdir -p ../data/svmlight/validation
mkdir -p ../data/svmlight/model
python transform-svmlight.py
../svmlight/svm_learn ../data/svmlight/train/train-svmlight.dat ../data/svmlight/model/svm_model
../svmlight/svm_classify ../data/svmlight/validation/validation-svmlight.dat ../data/svmlight/model/svm_model ../data/svmlight/model/validation.out


