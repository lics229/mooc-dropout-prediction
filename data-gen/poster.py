__author__ = 'dpeng'
from FeatureVectorGen import readFeatureFromCache
root_dir = '../data/final/v5/'

test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir +
                                                                              'test/FeatureVectorWithLabel.csv');
train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'train/FeatureVectorWithLabel.csv');
validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'validation/FeatureVectorWithLabel.csv');

import random
random.seed(1)
num_records = len(train_feature_matrix);
allo = [random.random() for i in range(0, num_records)];
import numpy as np
def sampleAtPercentage(train_target_vector, train_feature_matrix, allo, percentage):
    num_records = len(train_feature_matrix);
    indexes = [i for i in range(0, num_records) if allo[i] < percentage];
    num_samples = len(indexes);
    new_target = np.zeros((num_samples,));
    new_matrix = np.zeros((num_samples, len(train_feature_matrix[0])));
    for i, j in enumerate(indexes):
        new_target[i] = train_target_vector[j];
        for k in range(0, len(train_feature_matrix[0])):
            new_matrix[i][k] = train_feature_matrix[j][k];
    return (new_target, new_matrix)

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, f1_score, roc_curve, confusion_matrix

def calcMetrics(validation_target, prediction, scores):
    auc = roc_auc_score(validation_target, scores);
    roc = roc_curve(validation_target, scores);
    matrix = confusion_matrix(validation_target, prediction);
    f1_0 = f1_score(validation_target, prediction, pos_label=0);
    f1_1 = f1_score(validation_target, prediction, pos_label=1);
    return (auc, f1_0, f1_1, roc, matrix)

def trainLR(train_features, train_target, validation_features, validation_target):
    lambda_value = 0.1; # Best lambda for this dataset
    lr = LogisticRegression(penalty='l2', tol=1.0E-5, C=lambda_value, fit_intercept=True);
    lr.fit(train_features, train_target);
    scores = lr.predict_proba(validation_features)[:,1];
    prediction = lr.predict(validation_features);
    return calcMetrics(validation_target, prediction, scores)

from sklearn.ensemble import RandomForestClassifier
def trainRF(train_features, train_target, validation_features, validation_target):
    rf = RandomForestClassifier(n_estimators=400, criterion='entropy', max_features=0.8, max_depth=7)
    rf.fit(train_features, train_target);
    prediction = rf.predict(validation_features);
    scores = rf.predict_proba(validation_features)[:,1];
    return calcMetrics(validation_target, prediction, scores)

from sklearn.ensemble import GradientBoostingClassifier
def trainGB(train_features, train_target, validation_features, validation_target):
    rf = GradientBoostingClassifier(n_estimators=150, loss='deviance', max_features=0.6, learning_rate=0.05, max_depth=5)
    rf.fit(train_features, train_target);
    prediction = rf.predict(validation_features);
    scores = rf.predict_proba(validation_features)[:,1];
    return calcMetrics(validation_target, prediction, scores)

from sklearn.ensemble import AdaBoostClassifier
def trainAdaBoost(train_features, train_target, validation_features, validation_target):
    nt = 150;
    abc = AdaBoostClassifier(n_estimators=nt);
    abc.fit(train_features, train_target);
    prediction = abc.predict(validation_features);
    scores = abc.predict_proba(validation_features)[:,1];
    return calcMetrics(validation_target, prediction, scores)

def main():
    # pts = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];
    pts = [1.0];
    aucs = [];
    f1_0s = [];
    f1_1s = [];
    lr_metrics = [];
    rf_metrics = [];
    gb_metrics = [];
    for percentage in pts:
        print("Training %f" % percentage)
        (sample_target, sample_matrix) = sampleAtPercentage(train_target_vector, train_feature_matrix, allo, percentage);
        (auc, f10, f11, _, _) = trainLR(sample_matrix, sample_target, validation_feature_matrix, validation_target_vector);
        lr_metrics.append((percentage, auc, f10, f11));
        (auc, f10, f11, _, _) = trainRF(sample_matrix, sample_target, validation_feature_matrix, validation_target_vector);
        rf_metrics.append((percentage, auc, f10, f11));
        (auc, f10, f11, _, _) = trainGB(sample_matrix, sample_target, validation_feature_matrix, validation_target_vector);
        gb_metrics.append((percentage, auc, f10, f11));

    with open('../data/final/learning_curve/test/lr.dat', 'w') as lc:
        lc.write('#percentage auc f1_n f1_p\n')
        for i in range(0, len(pts)):
            lc.write('%f %f %f %f\n' % lr_metrics[i])

    with open('../data/final/learning_curve/test/random_forest.dat', 'w') as lc:
        lc.write('#percentage auc f1_n f1_p\n')
        for i in range(0, len(pts)):
            lc.write('%f %f %f %f\n' % rf_metrics[i])

    with open('../data/final/learning_curve/test/gb.dat', 'w') as lc:
        lc.write('#percentage auc f1_n f1_p\n')
        for i in range(0, len(pts)):
            lc.write('%f %f %f %f\n' % gb_metrics[i])

if __name__ == '__main__':
    main()
