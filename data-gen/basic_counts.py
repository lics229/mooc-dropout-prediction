#!/bin/env python

pos = 0;
neg = 0;
with open('../data/truth_train.csv') as file:
    file.next();
    for line in file:
        if (line.strip().split(",")[1] == '1'):
            pos += 1;
        else:
            neg += 1;

print('positive: %d, negative: %d' % (pos, neg))

# positive: 95581, negative: 24960
# Not very biased