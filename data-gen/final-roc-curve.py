
from poster import train_feature_matrix, train_target_vector, trainAdaBoost, trainGB, trainLR, trainRF, validation_feature_matrix, validation_target_vector
from evaluatesvmlight import evaluateSVMLightRoc

def main():
    output_file = '../data/final/roc/roc.dat';
    f = open(output_file, "w");
    methods = [trainGB, trainRF, trainAdaBoost, trainLR];
    for method in methods:
        print(str(method))
        (auc, f10, f11, roc, _) = method(train_feature_matrix, train_target_vector, validation_feature_matrix, validation_target_vector);
        f.write("# ROC for %s\n" % str(method));
        for i in range(len(roc[0])):
            f.write("%f %f\n" % (roc[0][i], roc[1][i]));
        f.write("\n");
    f.close();

def svmRoc():
    output_file = '../data/final/roc/svm-roc.dat';
    f = open(output_file, "w");
    scores_file = '/home/dpeng/study/cs229/mooc-dropout-prediction/data/final/svmlight/linear/c_0.001000_m_10000_q_50_n_25_v_3/predictions.out';
    roc = evaluateSVMLightRoc('../data/final/svmlight/validation_svm_light.dat', scores_file);
    f.write("# ROC for linear SVM\n");
    for i in range(len(roc[0])):
        f.write("%f %f\n" % (roc[0][i], roc[1][i]));
    f.write("\n");
    f.close();

if __name__ == '__main__':
    main()
    svmRoc()
