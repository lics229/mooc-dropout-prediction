__author__ = 'degao'

from FeatureVectorGen import readFeatureFromCache;
from sklearn.ensemble import GradientBoostingClassifier;
from sklearn.metrics import roc_auc_score;

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/final/v5'

test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/test/FeatureVectorWithLabel.csv');
train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/train/FeatureVectorWithLabel.csv');
validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/validation/FeatureVectorWithLabel.csv');

losses = ['deviance', 'exponential'];
learning_rates = [0.05, 0.1, 0.2];
n_ests = range(50, 550, 50);
depths = [3, 5, 7];
max_features = ['sqrt', 'log2', 0.2, 0.4, 0.6, 0.8, None];


for l in losses:
    for lr in learning_rates:
        for ne in n_ests:
            for d in depths:
                for mf in max_features:
                    args = "%s %f %d %d %s" % (l, lr, ne, d, str(mf));
                    print("Training %s" % args);
                    gbc = GradientBoostingClassifier(loss=l, learning_rate=lr, n_estimators=ne, max_depth=d, max_features=mf);
                    gbc.fit(train_feature_matrix, train_target_vector);
                    probs = gbc.predict_proba(validation_feature_matrix)[:,1];
                    auc = roc_auc_score(validation_target_vector, probs);
                    print("AUC: %f" % auc);
