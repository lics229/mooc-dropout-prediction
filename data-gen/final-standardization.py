__author__ = 'dpeng'
from FeatureVectorGen import readFeatureFromCache, saveFeatures
from sklearn.preprocessing import StandardScaler

versions = ['v1', 'v2', 'v3', 'v4', 'v5'];

for v in versions:
    workDir = "../data/final/%s/" % v;
    (train_target, train_features, names) = readFeatureFromCache(workDir + 'train/FeatureVectorWithLabel.csv');
    ss = StandardScaler();
    ss.fit(train_features);
    new_features = ss.transform(train_features);
    saveFeatures(train_target, new_features, names, workDir + 'train/normalized.csv');
    for other in ['validation', 'test']:
        input = '%s/%s/FeatureVectorWithLabel.csv' % (workDir, other);
        output = '%s/%s/normalized.csv' % (workDir, other);
        (target, features, names) = readFeatureFromCache(input);
        new_features = ss.transform(features);
        saveFeatures(target, new_features, names, output);

