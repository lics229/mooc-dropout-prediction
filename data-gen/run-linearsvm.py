rootDir = '../data/final/svmlight'
train_out_file = '%s/train_svm_light.dat' % rootDir;
validation_file = '%s/validation_svm_light.dat' % rootDir;

workDir = '../data/final/svmlight/linear'

import os
# os.mkdir(workDir)
# os.mkdir(workDir + "/model")
# os.mkdir(workDir + "/prediction")

from evaluatesvmlight import evaluateSVMLight

params = [1.0E-4, 1.0E-3, 1.0E-2, 1.0E-1, 1.0, 1.0E1, 1.0E2, 1.0E3, 1.0E4];

with open("%s/linear.metrics" % workDir, "w") as out:
    first = True;
    for lam in params:
        if lam is None:
            arg = '-m 10000 -q 50 -n 25 -v 3';
        else:
            arg = '-c %f -m 10000 -q 50 -n 25 -v 3' % lam;
        id = arg.replace("-", "").replace(" ", "_");
        print("Running %s" % id);
        outputDir = workDir + "/" + id;
        if (not os.path.isdir(outputDir)):
            os.mkdir(outputDir);
        model_file = outputDir + "/model.out";
        prediction_file = outputDir + "/predictions.out";
        alpha_file = outputDir + "/alpha.out";
        log_file = outputDir + "/log.out";
        arg += ' -a ' + alpha_file;
        if (first):
            first = False;
        else:
            arg += ' -y ' + previousAlpha;
        os.system("../svmlight/svm_learn %s %s %s > %s" % (arg, train_out_file, model_file, log_file))
        os.system("../svmlight/svm_classify %s %s %s" % (validation_file, model_file, prediction_file))
        auc = evaluateSVMLight(validation_file, prediction_file)
        out.write("%f:%s\n" % (auc, arg))
        print("%f:%s" % (auc, arg))
        previousAlpha = alpha_file;
