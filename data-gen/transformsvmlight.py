
# def transformSVMLight(input_file, output_file, whitelist_file):
#     feature_whitelist = set();
#     with open(whitelist_file) as wl_file:
#         for line in wl_file:
#             feature_whitelist.add(line.strip());
#     with open(input_file) as open_input_file, open(output_file, 'w') as open_output_file:
#         header = open_input_file.next().strip();
#         words = header.split(',');
#         whitelist_indexes = [index for index, word in enumerate(words) if word in feature_whitelist];
#         whitelist_list = [words[i] for i in whitelist_indexes];
#         output_header = '#label' + ' '.join(whitelist_list);
#         open_output_file.write(output_header + '\n');
#         for line in open_input_file:
#             words = line.strip().split(',');
#             label = int(words[1]) * 2 - 1;
#             features = [str(i) + ':' + words[i] for i in whitelist_indexes];
#             open_output_file.write(str(label) + ' ' + ' '.join(features) + '\n');

def transformSVMLightAll(input_file, output_file):
    open_input_file = open(input_file);
    open_output_file = open(output_file, 'w');
    header = open_input_file.next().strip();
    words = header.split(',');
    columns = len(words);
    output_header = '#label ' + ' '.join(words[2:]);
    open_output_file.write(output_header + '\n');
    for line in open_input_file:
        words = line.strip().split(',');
        label = float(words[1]) * 2 - 1;
        features = [str(i - 1) + ':' + words[i] for i in range(2, columns)];
        open_output_file.write(str(label) + ' ' + ' '.join(features) + '\n');
    open_input_file.close();
    open_output_file.close();

def __main__():
    input_file_name = 'FeatureVectorWithLabel.csv';
    train_input_file = '../data/final/v5/train/' + input_file_name;
    validation_input_file = '../data/final/v5/validation/' + input_file_name;
    test_input_file = '../data/final/v5/test/' + input_file_name;
    import os
    # os.mkdir('../data/final/svmlight');
    train_out_file = '../data/final/svmlight/train-svmlight.dat';
    validation_out_file = '../data/final/svmlight/validation-svmlight.dat';
    test_out_file = '../data/final/svmlight/test-svmlight.dat';
    transformSVMLightAll(train_input_file, train_out_file);
    transformSVMLightAll(validation_input_file, validation_out_file);
    transformSVMLightAll(test_input_file, test_out_file);

    import os

    for v in ['v1', 'v2', 'v3', 'v4', 'v5']:
        # os.mkdir('../data/final/%s/svmlight' % v);
        transformSVMLightAll('../data/final/%s/train/normalized.csv' % v, '../data/final/%s/svmlight/train_svm_light.dat' %
                             v);
        transformSVMLightAll('../data/final/%s/validation/normalized.csv' % v, '../data/final/%s/svmlight/validation_svm_light.dat' % v);
        transformSVMLightAll('../data/final/%s/test/normalized.csv' % v, '../data/final/%s/svmlight/test_svm_light.dat' % v);
