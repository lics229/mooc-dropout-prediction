import numpy as np;
import imp
Event = imp.load_source('Event', '../dataclasses/Event.py')
from sklearn import metrics;
import math;

SESSION_LENGTH_SECONDS = 1800; # Half an hour

# Classes to deal with sessionized features
class Session:
    def __init__(self):
        self.events = [];
    def shouldContainsEvent(self, event):
        lastEvent = self.events[len(self.events) - 1];
        deltaT = (event.time - lastEvent.time).seconds;
        return deltaT < SESSION_LENGTH_SECONDS
    def addEvent(self, event):
        self.events.append(event);

class Sessions:
    def __init__(self):
        self.sessions = [];
    def addEvent(self, event):
        if (len(self.sessions) == 0):
            session = Session();
            session.addEvent(event);
            self.sessions.append(session);
        else:
            lastSession = self.sessions[len(self.sessions) - 1];
            if (lastSession.shouldContainsEvent(event)):
                lastSession.addEvent(event);
            else:
                session = Session();
                session.addEvent(event);
                self.sessions.append(session);

def PrintMetrics(test_target_vector, test_scores, test_proba):
    print (GetMetrics(test_target_vector, test_scores, test_proba));

def GetMetrics(test_target_vector, test_scores, test_proba):
    ret = "";
    ret += "AUC Score:" + str(metrics.roc_auc_score(test_target_vector, test_proba));
    conf_mat = metrics.confusion_matrix(test_target_vector, test_scores);
    ret +="\nConfusion matrix: \n";
    ret += str(conf_mat);
    ret += "\nF1 Score (negative):" + str(metrics.f1_score(test_target_vector, test_scores, pos_label=0));
    ret += "\nF1 Score (positive):" + str(metrics.f1_score(test_target_vector, test_scores, pos_label=1));
    return ret;

def formatKey(f_arg, *argv):
    ret = f_arg;
    for arg in argv:
        ret = ret + "_" + arg;
    return ret;

def activityPerDayOfWeekFeatureName(weekDay):
    return 'act_cnt_weekDay_%02d' % weekDay;

def activityPerHourFeatureName(hour):
    return 'act_cnt_hour_%02d' % hour;

def activityCountFeatureName(day):
    return 'act_cnt_day_%02d' % day;

def numSessionFeatureName(week):
    return "sessions_in_week_%d" % week;

def readFeatureFromCache(filepath):
    with open(filepath) as f:
        lines = f.read().splitlines();
        features = lines[0].split(',')[2:];
        cols = len(features);
        rows = len(lines) - 1; # for header
        target_vector = np.zeros(rows);
        feature_matrix = np.zeros((rows, cols));
        rowCounter = 0;
        for index in range(1, rows + 1):
            parts = lines[index].split(',');
            target_vector[rowCounter] = float(parts[1]);
            colCounter = 0;
            for colCounter in range(0, cols ):
                feature_matrix[rowCounter][colCounter] = float(parts[colCounter+2]);
            rowCounter += 1;
        return (target_vector, feature_matrix, features);

def saveFeatures(target, features, names, filepath):
    with open(filepath, "w") as f:
        f.write("#enrollment_id,label," + ','.join(names) + "\n");
        for i in range(0, len(features)):
            ar = features[i,:].tolist();
            fs = [str(s) for s in ar];
            f.write("-1,%f,%s\n" % (target[i], ','.join(fs)));



def FeaturePrep(dataSet_dir, object_map, course_map, cacheFile = "FeatureVectorWithLabel.csv"):
    print('Preparing data for %s' % dataSet_dir)
    enroll_file = dataSet_dir + 'enrollment_train.csv';
    log_file = dataSet_dir + 'log_train.csv';
    truth_file = dataSet_dir + 'truth_train.csv';
    training_data_file = dataSet_dir + cacheFile;

    return readFeatureFromCache(training_data_file); # use cached feature set

    # building feature list
    INTERCEPT = 'INTERCEPT'; # not used anymore
    feature_names = set();
    additional_Feature = ['parallel_enrollments', 'avg_chapter_delays', 'class_size'];
    static_feature_list = ['wiki', 'navigate', 'access'];
    ngram_feature_list = [];

    # 4-gram features
    for gram1 in ngram_feature_list:
        for gram2 in ngram_feature_list:
            feature_names.add(formatKey(gram1, gram2));
            for gram3 in ngram_feature_list:
                feature_names.add(formatKey(gram1, gram2, gram3));
                #for gram4 in ngram_feature_list:
                #    feature_names.add(formatKey(gram1, gram2, gram3, gram4));


    # week starts from sunday i.e. 0
    for day in range(0,7):
        feature_names.add(activityPerDayOfWeekFeatureName(day));
    for hour in range(0,24):
        feature_names.add(activityPerHourFeatureName(hour));
    for day in range(0,31):
        feature_names.add(activityCountFeatureName(day));
    for week in range(0,5):
        feature_names.add(numSessionFeatureName(week));
    for feature in additional_Feature:
        feature_names.add(feature);
    for feature in static_feature_list:
        feature_names.add(feature);
        feature_names.add(formatKey('server', feature));
        feature_names.add(formatKey('browser' , feature));
    for module_id in object_map.keys():
        moduleObj = object_map[module_id];
        #feature_names.add(moduleObj.category);
        feature_names.add(formatKey("server" , moduleObj.category));
        feature_names.add(formatKey("browser" , moduleObj.category));
        #feature_names.add(moduleObj.category + "_percent");
        feature_names.add(formatKey("server" , moduleObj.category + "_percent"));
        feature_names.add(formatKey("browser" , moduleObj.category + "_percent"));

    #print(feature_names);
    print("feature names ready");

    enrollment_map = {}; # enrollment_id => (student_id, course_id)
    studentEnrollmentsMapping = {};
    courseStudentsMapping = {};
    with open(enroll_file) as enrollment_file:
        enrollment_file.next();
        for line in enrollment_file:
            split = line.strip().split(',');
            id = int(split[0]);
            enrollment_map[id] = (split[1], split[2]);
            if(split[1] not in studentEnrollmentsMapping):
                studentEnrollmentsMapping[split[1]] = [];
            studentEnrollmentsMapping[split[1]].append(id);
            if(split[2] not in courseStudentsMapping):
                courseStudentsMapping[split[2]] = [];
            courseStudentsMapping[split[2]].append(id);

    # enrollment_id (int) => drop out or not
    label_set = {}
    # enrollment_id (int) => feature vector
    features_set = {};

    print("initializing");

    # fill intercept terms
    for enrollment_id in enrollment_map.keys():
        features_set[enrollment_id] = {};
        for feature in feature_names:
            features_set[enrollment_id][feature] = 0; # initialize all features to zero
        # features_set[enrollment_id][INTERCEPT] = 1;

    print("fetching labels");

    # fill label
    with open(truth_file) as label_file:
        for line in label_file:
            words = line.split(',');
            id = int(words[0]);
            # we should have enrollment <-> student mapping for every id
            assert(id in enrollment_map);
            label_set[id] = int(words[1]);

    print("starting 1st iteration");

    # enrollment_id => event_type_list
    enrollment_eventTypeList = {};
    # earliest time students visits a chapter
    enrollment_firstChapterWatch = {};
    enrollment_moduleVisited = {}; # to get unique percentage visits
    #assert len(label_set.keys) == len(enrollment_map.keys);
    with open(log_file) as open_log_file:
        open_log_file.next();
        for line in open_log_file:
            #construct event
            event = Event.Event(line);
            event.setCategory(object_map);
            #print ("%s,%s") % (event.enrollment_id, event.module_id);
            if(event.enrollment_id in enrollment_map):

                if(event.enrollment_id not in enrollment_moduleVisited):
                    enrollment_moduleVisited[event.enrollment_id] = set();
                if(event.enrollment_id not in enrollment_eventTypeList):
                    enrollment_eventTypeList[event.enrollment_id] = [];

                # appending events in list for post processing
                if(formatKey(event.source, event.event_type) in ngram_feature_list):
                    enrollment_eventTypeList[event.enrollment_id].append(formatKey(event.source, event.event_type));

                course_id = enrollment_map[event.enrollment_id][1];
                day = (event.time - course_map[course_id].start).days + 1

                features_set[event.enrollment_id][activityCountFeatureName(day)] += 1;
                features_set[event.enrollment_id][activityPerHourFeatureName(event.time.hour)] += 1;
                features_set[event.enrollment_id][activityPerDayOfWeekFeatureName(event.time.weekday())] += 1;

                if(event.event_type in static_feature_list):
                    features_set[event.enrollment_id][event.event_type] += 1;
                    features_set[event.enrollment_id][formatKey(event.source, event.event_type)] += 1;
                if(event.category != 'UNKNOWN'):
                    if(event.module_id not in enrollment_moduleVisited[event.enrollment_id]):
                        enrollment_moduleVisited[event.enrollment_id].add(event.module_id);
                        totalModuleLikeInCourse = course_map[course_id].moduleCounts[event.category];
                        #features_set[event.enrollment_id][event.category + "_percent"] += 100.0/(totalModuleLikeInCourse);
                        features_set[event.enrollment_id][formatKey(event.source, event.category + "_percent")] += 100.0/totalModuleLikeInCourse;

                    #features_set[event.enrollment_id][event.category] += 1;
                    features_set[event.enrollment_id][formatKey(event.source, event.category)] += 1;

                if(event.category == "chapter" and object_map[event.module_id].start != None):
                    if(event.enrollment_id not in enrollment_firstChapterWatch):
                        enrollment_firstChapterWatch[event.enrollment_id] = {};

                    if(event.module_id not in enrollment_firstChapterWatch[event.enrollment_id]):
                        enrollment_firstChapterWatch[event.enrollment_id][event.module_id] = event.time;
                    elif(enrollment_firstChapterWatch[event.enrollment_id][event.module_id] > event.time):
                        enrollment_firstChapterWatch[event.enrollment_id][event.module_id] = event.time;

    print("starting 2nd iteration");

    # number of parallel features
    for enrollment_id in enrollment_map:
        studentId = enrollment_map[enrollment_id][0];
        courseId = enrollment_map[enrollment_id][1];
        features_set[enrollment_id]["class_size"] = len(courseStudentsMapping[courseId]);
        counter = 0;
        course = course_map[courseId];
        for other_enrollments in studentEnrollmentsMapping[studentId]:
            otherCourse = course_map[enrollment_map[other_enrollments][1]];
            if((course.start < otherCourse.start and otherCourse.start < course.end) or
               (otherCourse.start < course.start and course.start < otherCourse.end)):
                counter += 1;
        features_set[enrollment_id]["parallel_enrollments"] = counter;

        if(enrollment_id in enrollment_eventTypeList):
            for idx, val in enumerate(enrollment_eventTypeList[enrollment_id]):
                concerned_list = enrollment_eventTypeList[enrollment_id];
                if(idx + 3 < len(concerned_list) and
                   concerned_list[idx] in ngram_feature_list and
                   concerned_list[idx+1] in ngram_feature_list and
                   concerned_list[idx+2] in ngram_feature_list):
                    featureKey = formatKey(concerned_list[idx], concerned_list[idx+1], concerned_list[idx+2]);
                    features_set[enrollment_id][featureKey] += 1;

                if(idx + 2 < len(concerned_list) and
                   concerned_list[idx] in ngram_feature_list and
                   concerned_list[idx+1] in ngram_feature_list):
                    featureKey = formatKey(concerned_list[idx], concerned_list[idx+1]);
                    features_set[enrollment_id][featureKey] += 1;

        if(enrollment_id in enrollment_firstChapterWatch):
            TotalDays = 0.0;
            for module_id in enrollment_firstChapterWatch[enrollment_id].keys():
                TotalDays += (enrollment_firstChapterWatch[enrollment_id][module_id] - object_map[module_id].start).days + 1
            if TotalDays > 0.0: # data is not consistent as time travel is still an open problem
                features_set[enrollment_id]["avg_chapter_delays"] = (len(enrollment_firstChapterWatch[enrollment_id])/TotalDays);

    print("Gather sessionized features")
    sessionized_features = genSessionizedFeatures(dataSet_dir, course_map);
    for key, value in sessionized_features.iteritems():
        for i, v in enumerate(value):
            feature_name = numSessionFeatureName(i);
            features_set[key][feature_name] = v;

    print("converting to matrix");

    feature_name_list = list(feature_names);
    header_str = ('%s,%s,%s') % ('enrollment_id', 'label', ','.join(feature_name_list));
    target_vector = np.zeros(len(enrollment_map));
    rowNum = 0;
    feature_matrix = np.zeros((len(enrollment_map), len(feature_name_list)));

    with open(training_data_file, 'w') as data_file:
        data_file.write(header_str + '\n');
        for enrollment_id in enrollment_map.keys():
            line = [];
            line.append(str(enrollment_id));
            line.append(str(label_set[enrollment_id]));
            target_vector[rowNum] = label_set[enrollment_id];
            columnNum = 0;
            for feature in feature_name_list:
                line.append(str(features_set[enrollment_id][feature]));
                feature_matrix[rowNum][columnNum] = features_set[enrollment_id][feature];
                columnNum += 1;
            data_file.write(','.join(line) + '\n');
            rowNum = rowNum + 1;
    return (target_vector, feature_matrix, feature_name_list)

def genSessionizedFeatures(dataSet_dir, course_map):
    print('Preparing sessionized data for %s' % dataSet_dir)
    log_file = dataSet_dir + 'log_train.csv';
    enroll_file = dataSet_dir + 'enrollment_train.csv';
    enrollment_map = {}; # enrollment_id => (student_id, course_id)
    with open(enroll_file) as enrollment_file:
        enrollment_file.next();
        for line in enrollment_file:
            split = line.strip().split(',');
            id = int(split[0]);
            enrollment_map[id] = (split[1], split[2]);
    full_feature_map = {};
    with open(log_file) as open_log_file:
        open_log_file.next();
        all_sessions = [Sessions(), Sessions(), Sessions(), Sessions(), Sessions()];
        previous_enrollment_id = -1;
        for line in open_log_file:
            #construct event
            event = Event.Event(line);
            if (previous_enrollment_id != -1 and event.enrollment_id != previous_enrollment_id):
                full_feature_map[previous_enrollment_id] = [len(x.sessions) for x in all_sessions];
                all_sessions = [Sessions(), Sessions(), Sessions(), Sessions(), Sessions()];
            else:
                course_id = enrollment_map[event.enrollment_id][1];
                day = (event.time - course_map[course_id].start).days;
                bucket = day / 7;
                sessions = all_sessions[bucket];
                sessions.addEvent(event);
            previous_enrollment_id = event.enrollment_id;
    return full_feature_map;





