__author__ = 'degao'

from sklearn import metrics;
from FeatureVectorGen import FeaturePrep;

from FeatureVectorGen import PrintMetrics;
from pprint import pprint

from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
import numpy as np
import matplotlib.pyplot as plt


# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/complete/'

test_target_vector, test_feature_matrix, feature_names = FeaturePrep(root_dir + '../test/', object_map, course_map);
train_target_vector, train_feature_matrix, feature_names = FeaturePrep(root_dir + '../train/', object_map, course_map);
validation_target_vector, validation_feature_matrix, feature_names = FeaturePrep(root_dir + '../validation/', object_map, course_map);

print('Start Adaboost training')

algo = AdaBoostClassifier();
algo = algo.fit(train_feature_matrix, train_target_vector);


importances = algo.feature_importances_
std = np.std([tree.feature_importances_ for tree in algo.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

new_names = [];
for f in range(len(feature_names)):
    new_names.append(feature_names[indices[f]]);
    print("%d. feature %s (%f)" % (f + 1, feature_names[indices[f]], importances[indices[f]]))

# http://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_importances.html
# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(train_feature_matrix.shape[1]), importances[indices], color="r", yerr=std[indices], align="center");
plt.xticks(range(len(new_names)), new_names)
plt.xlim([-1, len(new_names)])
plt.show()

print ("\nTest Set Metrics");
PrintMetrics(test_target_vector, algo.predict(test_feature_matrix), algo.predict_proba(test_feature_matrix)[:,1]);
print ("\nValidation Set Metrics");
PrintMetrics(validation_target_vector, algo.predict(validation_feature_matrix), algo.predict_proba(validation_feature_matrix)[:,1]);
