__author__ = 'dpeng'
from FeatureVectorGen import readFeatureFromCache
root_dir = '../data/poster/'

test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir +
                                                                              'test/FeatureVectorWithLabel.csv');

train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'train/FeatureVectorWithLabel.csv');

