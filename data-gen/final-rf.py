__author__ = 'degao'

from FeatureVectorGen import readFeatureFromCache;
from sklearn.ensemble import RandomForestClassifier;
from sklearn.metrics import roc_auc_score;

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/final/v5'

test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/test/FeatureVectorWithLabel.csv');
train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/train/FeatureVectorWithLabel.csv');
validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/validation/FeatureVectorWithLabel.csv');

criteria = ['gini', 'entropy'];
n_ests = range(50, 550, 50);
depths = [3, 5, 7, None];
max_features = ['sqrt', 'log2', 0.2, 0.4, 0.6, 0.8, None];


for l in criteria:
    for ne in n_ests:
        for d in depths:
            for mf in max_features:
                args = "%s %d %s %s" % (l, ne, str(d), str(mf));
                print("Training %s" % args);
                rfc = RandomForestClassifier(criterion=l, n_estimators=ne, max_depth=d, max_features=mf);
                rfc.fit(train_feature_matrix, train_target_vector);
                probs = rfc.predict_proba(validation_feature_matrix)[:,1];
                auc = roc_auc_score(validation_target_vector, probs);
                print("AUC: %f" % auc);

