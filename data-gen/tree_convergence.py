__author__ = 'dpeng'
from FeatureVectorGen import readFeatureFromCache

from poster import calcMetrics

from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier

def main():
    root_dir = '../data/final/v5/'
    test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir +
                                                                                  'test/FeatureVectorWithLabel.csv');
    train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'train/FeatureVectorWithLabel.csv');
    validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'validation/FeatureVectorWithLabel.csv');
    min_estimator = 10;
    max_estimator = 500;
    increment = 10;
    rf = [RandomForestClassifier(criterion='entropy', max_features='auto', warm_start=True),
          RandomForestClassifier(criterion='gini', max_features='auto', warm_start=True),
          GradientBoostingClassifier(loss='deviance', max_features='auto', warm_start=True),
          GradientBoostingClassifier(loss='exponential', max_features='auto', warm_start=True),
          AdaBoostClassifier()];
    results = {};
    results['gini'] = [];
    results['entropy'] = [];
    results['deviance'] = [];
    results['exponential'] = [];
    results['AdaBoost'] = [];
    for num_t in range(min_estimator, max_estimator + increment, increment):
        for classifier in rf:
            classifier.set_params(n_estimators=num_t);
            classifier.fit(train_feature_matrix, train_target_vector);
            prediction = classifier.predict(validation_feature_matrix);
            scores = classifier.predict_proba(validation_feature_matrix)[:,1];
            (auc, f1_0, f1_1, _, _) = calcMetrics(validation_target_vector, prediction, scores);
            if (type(classifier) == RandomForestClassifier):
                id = classifier.criterion;
            elif (type(classifier) == GradientBoostingClassifier):
                id = classifier.loss;
            else:
                id = 'AdaBoost';
            res = (num_t, auc, f1_0, f1_1);
            print(res)
            results[id].append((num_t, auc, f1_0, f1_1));

    with open('../data/final/tree-convergence/convergence.dat', 'w') as lc:
        for key, value in results.iteritems():
            lc.write('#%s: num_tree auc f10 f11\n' % key)
            for item in value:
                lc.write('%d %f %f %f\n' % item);
            lc.write('\n');

if __name__ == '__main__':
    main()
