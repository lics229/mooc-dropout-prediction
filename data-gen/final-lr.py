__author__ = 'degao'

from FeatureVectorGen import readFeatureFromCache;
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score;

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/final/v5'

test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/test/normalized.csv');
train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/train/normalized.csv');
validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/validation/normalized.csv');

params = [1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0E5];
import sys
for p in params:
    print("Training %f" % p);
    cl = LogisticRegression(C=p);
    cl.fit(train_feature_matrix, train_target_vector);
    probs = cl.predict_proba(validation_feature_matrix)[:,1];
    auc = roc_auc_score(validation_target_vector, probs);
    print("AUC: %f" % auc);
    sys.stdout.flush()

