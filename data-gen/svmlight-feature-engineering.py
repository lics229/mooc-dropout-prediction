import os
from evaluatesvmlight import evaluateSVMLight
rootDir = '../data/final';
aucs = [];
for v in ['v1', 'v2', 'v3', 'v4', 'v5']:
    print("Training %s" % v);
    workingDir = '%s/%s/svmlight' % (rootDir, v);
    os.system("../svmlight/svm_learn -m 5000 %s %s" % (workingDir + '/train_svm_light.dat', workingDir + '/basic_model'));
    os.system("../svmlight/svm_classify %s %s %s" % (workingDir + '/validation_svm_light.dat', workingDir + '/basic_model', workingDir + '/prediction.out'));
    auc = evaluateSVMLight(workingDir + '/validation_svm_light.dat', workingDir + '/prediction.out');
    aucs.append(auc);

with open("../data/final/feature-engineering/svmlight-normalized.dat", "w") as open_file:
    open_file.write("# SVM auc using svmlight\n");
    for i, a in enumerate(aucs):
        open_file.write("%d %f\n" % (i + 1, a));
