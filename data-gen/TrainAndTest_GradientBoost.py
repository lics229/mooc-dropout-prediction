__author__ = 'degao'

from sklearn import metrics;
from FeatureVectorGen import FeaturePrep;

from FeatureVectorGen import PrintMetrics;
from pprint import pprint

from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
from sklearn.ensemble import GradientBoostingClassifier

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/complete/'

test_target_vector, test_feature_matrix, feature_names = FeaturePrep(root_dir + '../test/', object_map, course_map);
train_target_vector, train_feature_matrix, feature_names = FeaturePrep(root_dir + '../train/', object_map, course_map);
validation_target_vector, validation_feature_matrix, feature_names = FeaturePrep(root_dir + '../validation/', object_map, course_map);

print('Start Gradient training')

algo = GradientBoostingClassifier(n_estimators=250, max_depth=3);
algo = algo.fit(train_feature_matrix, train_target_vector);

print ("\nTest Set Metrics");
PrintMetrics(test_target_vector, algo.predict(test_feature_matrix), algo.predict_proba(test_feature_matrix)[:,1]);
print ("\nValidation Set Metrics");
PrintMetrics(validation_target_vector, algo.predict(validation_feature_matrix), algo.predict_proba(validation_feature_matrix)[:,1]);