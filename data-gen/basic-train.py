__author__ = 'dpeng'
input_file = '../data/basic-train.csv';
train_file = '../data/basic/train.csv';
test_file = '../data/basic/test.csv';
split_ratio = 0.8;

from random import random

num_train = 0;
num_test = 0;
with open(input_file) as basic_training, \
     open(train_file, 'w') as train_data, \
     open(test_file, 'w') as test_data:
    basic_training.next();
    for line in basic_training:
        if (random() <= split_ratio):
            train_data.write(line);
            num_train += 1;
        else:
            test_data.write(line);
            num_test += 1;

import numpy as np

def read_data(file_name):
    num_lines = 0;
    num_feature = -1;
    with open(file_name) as file:
        for line in file:
            num_lines += 1;
            if (num_feature == -1):
                num_feature = len(line.strip().split(',')) - 2;
    feature_matrix = np.ndarray((num_lines, num_feature));
    target_vector = np.ndarray((num_lines,));
    with open(file_name) as file:
        ind = 0;
        for line in file:
            words = line.strip().split(',');
            numbers = [float(x) for x in words];
            target_vector[ind] = words[1];
            feature_matrix[ind] = np.ndarray(numbers[2:]);
    return (feature_matrix, target_vector);

(train_feature, train_vector) = read_data(train_file);
(test_feature, test_vector) = read_data(train_file);

from sklearn.linear_model import LogisticRegression

lr = LogisticRegression();
model = lr.fit(train_feature, train_vector);
model.predict(test_feature, test_vector);


