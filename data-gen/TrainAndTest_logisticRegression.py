__author__ = 'degao'

from sklearn.linear_model import LogisticRegression
from sklearn import metrics, preprocessing;
from FeatureVectorGen import FeaturePrep;

from FeatureVectorGen import PrintMetrics;
from pprint import pprint

from sklearn.linear_model import LogisticRegression
from sklearn import metrics

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/poster/'

test_target_vector, test_feature_matrix, feature_names = FeaturePrep(root_dir + 'test/', object_map, course_map);
train_target_vector, train_feature_matrix, feature_names = FeaturePrep(root_dir + 'train/', object_map, course_map);
validation_target_vector, validation_feature_matrix, feature_names = FeaturePrep(root_dir + 'validation/', object_map, course_map);


# from sklearn import preprocessing
# normalizer = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True);
# normalizer.fit(train_feature_matrix);
# normalizer.transform(train_feature_matrix, copy=False);
# normalizer.transform(test_feature_matrix, copy=False);
# normalizer.transform(validation_feature_matrix, copy=False);


print('Start LogisticRegression training')
lambdas = [1.0E-4, 1.0E-3, 1.0E-2, 1.0E-1, 1.0, 1.0E2, 1.0E3];
models = [];
for lambda_value in lambdas:
    lr = LogisticRegression(penalty='l2', tol=1.0E-5, C=lambda_value, fit_intercept=True);
    model = lr.fit(train_feature_matrix, train_target_vector);
    scores = lr.predict_proba(validation_feature_matrix)[:,1];
    auc = metrics.roc_auc_score(validation_target_vector, scores);
    models.append((lambda_value, lr, model, auc, 'Logistic regression'));


# from sklearn import svm
# for lambda_value in lambdas:
#     clf = svm.SVC(C=lambda_value, kernel='rbf', probability=True, cache_size=3000, verbose=True);
#     model = clf.fit(train_feature_matrix, train_target_vector);
#     scores = clf.predict_proba(validation_feature_matrix)[:,1];
#     auc = metrics.roc_auc_score(validation_target_vector, scores);
#     models.append((lambda_value, clf, model, auc, 'SVM-rbf'));
#
# for lambda_value in lambdas:
#     clf = svm.SVC(C=lambda_value, kernel='poly', probability=True);
#     model = clf.fit(train_feature_matrix, train_target_vector);
#     scores = clf.predict_proba(validation_feature_matrix)[:,1];
#     auc = metrics.roc_auc_score(validation_target_vector, scores);
#     models.append((lambda_value, clf, model, auc, 'SVM-poly-3'));

# Get the best model using the roc auc
print("%10s%10s%20s" % ("lambda", "roc_auc", "model"))

for x, y, z, w, v in models:
    print("%10f%10f%20s" % (x, w, v));

best_model = max(models, key=lambda model:model[3]);
algo = best_model[1];

coefs = best_model[1].coef_.tolist()[0];
cn = zip(coefs, feature_names);
cn.sort(key=lambda x:abs(x[0]));
for coef, name in cn:
    print(str(coef) + ',' + name);
    
print("Best model: lambda=%f" % best_model[0]);

print "\nTest Set Metrics";
PrintMetrics(test_target_vector, algo.predict(test_feature_matrix), algo.predict_proba(test_feature_matrix)[:,1]);
print "\nValidation Set Metrics";
PrintMetrics(validation_target_vector, algo.predict(validation_feature_matrix), algo.predict_proba(validation_feature_matrix)[:,1]);

