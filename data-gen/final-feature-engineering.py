__author__ = 'degao'
from FeatureVectorGen import readFeatureFromCache
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score

def v2n(s):
    return int(s[1]);

# baseName = 'FeatureVectorWithLabel.csv';
baseName = 'normalized.csv';
versions = ['v1', 'v2', 'v3', 'v4', 'v5'];
rootDir = '../data/final';
classifiers = [LogisticRegression(), AdaBoostClassifier(), GradientBoostingClassifier(), RandomForestClassifier()];
results = {};
for classifier in classifiers:
    s = str(type(classifier));
    result = [];
    for v in versions:
        vdir = '%s/%s' % (rootDir, v);
        train_file = '%s/train/%s' % (vdir, baseName);
        validation_file = '%s/validation/%s' % (vdir, baseName);
        test_file = '%s/test/%s' % (vdir, baseName);
        (train_target, train_matrix, names) = readFeatureFromCache(train_file);
        (validation_target, validation_matrix, names) = readFeatureFromCache(validation_file);
        (test_target, test_matrix, names) = readFeatureFromCache(test_file);
        num_features = len(names);
        print("Training %s on %s" % (str(classifier), v))
        classifier.fit(train_matrix, train_target);
        scores = classifier.predict_proba(validation_matrix)[:,1];
        auc = roc_auc_score(validation_target, scores);
        print("AUC %f" % auc);
        result.append(auc);
    results[s] = result;

with open('../data/final/feature-engineering/auc-normalized.dat', 'w') as open_file:
    for cl, val in results.iteritems():
        open_file.write("#classifier %s\n" % cl);
        for i, v in enumerate(val):
            open_file.write("%d %f\n" % (i + 1, v));
        open_file.write("\n");

