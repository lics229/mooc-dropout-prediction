__author__ = 'degao'

from sklearn.linear_model import LogisticRegression
from sklearn import metrics;
from FeatureVectorGen import FeaturePrep;

from FeatureVectorGen import PrintMetrics;
from pprint import pprint

from sklearn import svm
from sklearn import metrics

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/complete/'

test_target_vector, test_feature_matrix, feature_names = FeaturePrep(root_dir + '../test/', object_map, course_map);
train_target_vector, train_feature_matrix, feature_names = FeaturePrep(root_dir + '../train/', object_map, course_map);
validation_target_vector, validation_feature_matrix, feature_names = FeaturePrep(root_dir + '../validation/', object_map, course_map);

print('Start SVM training')

lambdas = [0.1];
for lambda_value in lambdas:
     clf = svm.SVC(C=lambda_value, kernel='linear', cache_size=6000, verbose=True, probability=True);
     print "trained"
     model = clf.fit(train_feature_matrix, train_target_vector);
     print "fitted"
     scores = clf.predict_proba(validation_feature_matrix)[:,1];
     auc = metrics.roc_auc_score(validation_target_vector, scores);
     models.append((lambda_value, clf, model, auc, 'SVM-rbf'));

# for lambda_value in lambdas:
#     clf = svm.SVC(C=lambda_value, kernel='poly', probability=True);
#     model = clf.fit(train_feature_matrix, train_target_vector);
#     scores = clf.predict_proba(validation_feature_matrix)[:,1];
#     auc = metrics.roc_auc_score(validation_target_vector, scores);
#     models.append((lambda_value, clf, model, auc, 'SVM-poly-3'));

# Get the best model using the roc auc
print("%10s%10s%20s" % ("lambda", "roc_auc", "model"))

for x, y, z, w, v in models:
    print("%10f%10f%20s" % (x, w, v));

best_model = max(models, key=lambda model:model[3]);
algo = best_model[1];

print("Best model: lambda=%f" % best_model[0]);

print "\nTest Set Metrics";
PrintMetrics(test_target_vector, algo.predict(test_feature_matrix), algo.predict_proba(test_feature_matrix)[:,1]);
print "\nValidation Set Metrics";
PrintMetrics(validation_target_vector, algo.predict(validation_feature_matrix), algo.predict_proba(validation_feature_matrix)[:,1]);
