#!/usr/bin/env python

import imp
ClassObject = imp.load_source('ClassObject', '../dataclasses/ClassObject.py')
Course = imp.load_source('Course', '../dataclasses/Course.py')

objectsFile = '../data/ObjectData/object.csv';
date_file = '../data/ObjectData/date.csv';

print "Reading obects from "+ objectsFile;

# Execution program starts here onwards

# read and build node
with open(objectsFile) as openfileobject:
    openfileobject.next();
    objects = [ClassObject.ClassObject(line) for line in openfileobject];

# build tree
object_map = dict([(obj.module_id, obj) for obj in objects]);

detached_objects = 0;

# linking children links to parent
for obj in objects:
    valid_children = [];
    for child in obj.children:
        if (child in object_map):
            child_node = object_map[child];
            child_node.parent = obj;
            obj.children_nodes.append(child_node);
            valid_children.append(child);
        else:
            detached_objects += 1;
    obj.children = valid_children;


# objects with no parent
module_roots = set([value.module_id for value in object_map.values() if value.parent is None]);
# dedupped courses
courses = [Course.Course(y) for y in set([x.course_id for x in objects])];
# course_id => course object mapping
course_map = dict([(x.course_id, x) for x in courses]);

# set course start end date
with open(date_file) as open_date_file:
    open_date_file.next();
    for line in open_date_file:
        words = line.strip().split(',');
        course_id = words[0];
        course = course_map[course_id];
        course.setDate(words[1], words[2]);

for module_root in module_roots:
    module = object_map[module_root];
    course = course_map[module.course_id];
    course.addChild(object_map[module_root]);

# Category analysis
# >>> set([object_map[module].category for module in module_roots])
# set(['chapter', 'course_info', 'about', 'vertical', 'discussion', 'outlink', 'static_tab', 'html', 'course', 'sequential', 'video', 'problem'])
# >>> set([value.category for value in object_map.values()])
# set(['chapter', 'course_info', 'about', 'vertical', 'discussion', 'outlink', 'static_tab', 'html', 'peergrading', 'course', 'combinedopenended', 'sequential', 'video', 'dictation', 'problem'])
# >>> set([x.category for x in object_map.values() if len(x.children) == 0])
# set(['chapter', 'course_info', 'about', 'sequential', 'vertical', 'discussion', 'outlink', 'static_tab', 'peergrading', 'combinedopenended', 'html', 'video', 'dictation', 'problem'])

# Top level module:
# 'chapter', 'course_info', 'about', 'vertical', 'discussion', 'outlink', 'static_tab', 'html', 'course', 'sequential', 'video', 'problem'
# Sublevel module:
# 'combinedopenended', 'dictation', 'peergrading'
categories = set([value.category for value in object_map.values()])
category_children_map = {};
for category in categories:
    child_categories = set([y.category for x in objects if x.category == category for y in x.children_nodes]);
    category_children_map[category] = child_categories;
# {'chapter': set(['sequential']), 'course_info': set([]), 'about': set([]), 'sequential': set(['vertical']), 'vertical': set(['discussion', 'outlink', 'peergrading', 'combinedopenended', 'html', 'video', 'dictation', 'problem']), 'discussion': set([]), 'outlink': set([]), 'static_tab': set([]), 'peergrading': set([]), 'course': set(['chapter']), 'combinedopenended': set([]), 'html': set([]), 'video': set([]), 'dictation': set([]), 'problem': set([])}
# Canonical course object structure:
# course\chapter\sequential\vertical\{'discussion', 'outlink', 'peergrading', 'combinedopenended', 'html', 'video', 'dictation', 'problem'}
#>>> [y.children for x in courses for y in x.children if y.category == 'chapter']
#[[], [], [], [], []]
# If top level module is 'chapter' or 'sequential', it has no children.

# accumulate module category stats at course level
for module_id in object_map:
    module = object_map[module_id];
    course_map[module.course_id].addModuleCount(module.category);

print "Finished reading objects"
