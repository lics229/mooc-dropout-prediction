
rootDir = '../data/final/svmlight'
train_out_file = '%s/train/train-svmlight.dat' % rootDir;
validation_file = '%s/validation/validation-svmlight.dat' % rootDir;

import os

from evaluatesvmlight import evaluateSVMLight

params = [1.0E-1, 1.0E-2, 1.0E-3, 1.0E-4, 1.0E-5, 1.0E-6];

os.system("mkdir -p %s/metrics" % rootDir)
os.system("mkdir -p %s/model/sigmoid" % rootDir)
consts = [0, 0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0];
factors = [0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0];

print("Sigmoid warm start");
with open("%s/metrics/sigmoid.metrics" % rootDir, "w") as out:
    first = True;
    for c in consts:
        for s in factors:
            for lam in params:
                arg = '-c %f -m 10000 -t 3 -s %f -r %f' % (lam, s, c);
                id = arg.replace("-", "").replace(" ", "_");
                model_file = '%s/model/sigmoid/%s' % (rootDir, id);
                prediction_file = '%s/model/sigmoid/prediction-%s' % (rootDir, id);
                alpha_file = '%s/model/sigmoid/alpha-%s' % (rootDir, id);
                arg += ' -a ' + alpha_file;
                if (first):
                    first = False;
                else:
                    arg += ' -y ' + previousAlpha;
                os.system("../svmlight/svm_learn %s %s %s" % (arg, train_out_file, model_file))
                os.system("../svmlight/svm_classify %s %s %s" % (validation_file, model_file, prediction_file))
                auc = evaluateSVMLight(validation_file, prediction_file)
                out.write("%f:%s" % (auc, arg))
                previousAlpha = alpha_file;



