from FeatureVectorGen import readFeatureFromCache
from math import sqrt
(target, features, names) = readFeatureFromCache('../data/final/v5/train/FeatureVectorWithLabel.csv');
print(names[42])
import numpy as np
s = 0.0;
for i in range(0, len(features)):
    v = features[i];
    d = np.dot(v, v);
    # if (sqrt(d)>3000):
    #     print(v);
    s += sqrt(d);

estimate = 1 / (s / len(features));
print("Estimated %f" % estimate);