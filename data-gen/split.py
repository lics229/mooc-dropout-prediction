#split files
ids = ['RF entropy', 'RF gini', 'GBDT deviance', 'GBDT exponential', 'AdaBoost'];
files = [open(x + '.out', 'w') for x in ids];
input = open('tree-convergence.out');
input.next();
ind = 0;
for line in input:
    a = line.strip().strip('(').strip(')');
    bes = ind % 5;
    files[bes].write(a + "\n");
    ind += 1;

for file in files:
    file.close();
