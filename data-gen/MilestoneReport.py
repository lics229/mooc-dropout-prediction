import numpy as np
def readFeatureLabel(input_file, whitelist_file):
    feature_whitelist = set();
    with open(whitelist_file) as wl_file:
        for line in wl_file:
            feature_whitelist.add(line.strip());
    num_whitelist = len(feature_whitelist);
    num_records = 0;
    with open(input_file) as open_input_file:
         header = open_input_file.next().strip();
         words = header.split(',');
         whitelist_indexes = [index for index, word in enumerate(words) if word in feature_whitelist];
         whitelist_list = [words[i] for i in whitelist_indexes];
         for line in open_input_file:
             num_records += 1;
    feature_matrix = np.zeros((num_records, num_whitelist));
    target_vector = np.zeros((num_records,));
    with open(input_file) as open_input_file:
        open_input_file.next();
        i = 0;
        for line in open_input_file:
            words = line.strip().split(',');
            target_vector[i] = float(words[1]);
            for j, index in enumerate(whitelist_indexes):
                x = float(words[index]);
                feature_matrix[i][j] = x;
            i += 1;
    return (feature_matrix, target_vector, whitelist_list);

def trainDataLR(train_feature_matrix, train_target_vector, validation_feature_matrix, validation_target_vector,
                test_feature_matrix, test_target_vector, feature_names):
    from sklearn import preprocessing
    normalizer = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True);
    normalizer.fit(train_feature_matrix);
    train_feature_normalized = normalizer.transform(train_feature_matrix, copy=True);
    test_feature_normalized = normalizer.transform(test_feature_matrix, copy=True);
    validation_feature_normalized = normalizer.transform(validation_feature_matrix, copy=True);
    from sklearn.linear_model import LogisticRegression
    from sklearn import metrics
    lambdas = [1.0E-4, 1.0E-3, 1.0E-2, 1.0E-1, 1.0, 1.0E2, 1.0E3];
    models = [];
    for lambda_value in lambdas:
        lr = LogisticRegression(penalty='l2', tol=1.0E-5, C=lambda_value, fit_intercept=True);
        lr.fit(train_feature_normalized, train_target_vector);
        scores = lr.predict_proba(validation_feature_normalized)[:,1];
        auc = metrics.roc_auc_score(validation_target_vector, scores);
        models.append((lambda_value, lr, auc, 'Logistic regression'));
    print("%10s%10s%20s" % ("lambda", "roc_auc", "model"))

    for x, y, z, w in models:
        print("%10f%10f%20s" % (x, z, w));

    best_model = max(models, key=lambda model:model[3]);
    algo = best_model[1];

    coefs = best_model[1].coef_.tolist()[0];
    cn = zip(coefs, feature_names);
    cn.sort(key=lambda x:abs(x[0]));
    for coef, name in cn:
        print(str(coef) + ',' + name);
    print(str(algo.intercept_) + ',INTERCEPT');
    scores = algo.predict_proba(validation_feature_normalized)[:,1];


    print("Best model: lambda=%f" % best_model[0]);
    from FeatureVectorGen import PrintMetrics
    print "Test Set Metrics";
    PrintMetrics(test_target_vector, algo.predict(test_feature_normalized), \
                 algo.predict_proba(test_feature_normalized)[:,1]);
    print "\nValidation Set Metrics";
    PrintMetrics(validation_target_vector, algo.predict(validation_feature_normalized), \
                 algo.predict_proba(validation_feature_normalized)[:,1]);
    return metrics.roc_curve(validation_target_vector, scores);


input_file_name = 'FeatureVectorWithLabel.csv';
v1_wl_file = '../data/report/v1.features';
v2_wl_file = '../data/report/v2.features';
v3_wl_file = '../data/report/v3.features';
wl_files = [v1_wl_file, v2_wl_file, v3_wl_file];
train_input_file = '../data/train/' + input_file_name;
validation_input_file = '../data/validation/' + input_file_name;
test_input_file = '../data/test/' + input_file_name;
roc_curves = [];
for wl_file in wl_files:
    print('Analysing ' + wl_file);
    train_feature_matrix, train_target_vector, whitelist_list = readFeatureLabel(train_input_file, wl_file);
    validation_feature_matrix, validation_target_vector, whitelist_list = readFeatureLabel(validation_input_file, wl_file);
    test_feature_matrix, test_target_vector, whitelist_list = readFeatureLabel(test_input_file, wl_file);
    x = trainDataLR(train_feature_matrix, train_target_vector, validation_feature_matrix, validation_target_vector,\
                test_feature_matrix, test_target_vector, whitelist_list);
    roc_curves.append(x);

import matplotlib.pyplot as plt
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
for version, (x, y, z) in enumerate(roc_curves):
    plt.plot(x, y, label='v%d' % (version + 1));

plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
# plt.show();
plt.savefig('../data/report/roc.pdf');

