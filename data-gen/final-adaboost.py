__author__ = 'degao'

from FeatureVectorGen import readFeatureFromCache;
from sklearn.ensemble import AdaBoostClassifier;
from sklearn.metrics import roc_auc_score;

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/final/v5'

test_target_vector, test_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/test/FeatureVectorWithLabel.csv');
train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/train/FeatureVectorWithLabel.csv');
validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + '/validation/FeatureVectorWithLabel.csv');

n_ests = range(50, 550, 50);

for ne in n_ests:
    args = ne;
    print("Training %s" % args);
    abc = AdaBoostClassifier(n_estimators=ne);
    abc.fit(train_feature_matrix, train_target_vector);
    probs = abc.predict_proba(validation_feature_matrix)[:,1];
    auc = roc_auc_score(validation_target_vector, probs);
    print("AUC: %f" % auc);

