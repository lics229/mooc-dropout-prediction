__author__ = 'degao'

from sklearn import metrics;
from FeatureVectorGen import FeaturePrep;

from FeatureVectorGen import PrintMetrics;
from pprint import pprint
from sklearn import metrics;
from sklearn import metrics
from sklearn.ensemble import GradientBoostingClassifier
from time import time
from sklearn.grid_search import GridSearchCV

# Build object and course map
execfile('ReadObjectsData.py');

root_dir = '../data/complete/'

test_target_vector, test_feature_matrix, feature_names = FeaturePrep(root_dir + '../test/', object_map, course_map);
train_target_vector, train_feature_matrix, feature_names = FeaturePrep(root_dir + '../train/', object_map, course_map);
validation_target_vector, validation_feature_matrix, feature_names = FeaturePrep(root_dir + '../validation/', object_map, course_map);

print('Start Gradient training')

                        
param_grid = {"min_samples_split" : [2, 50], 
              "min_samples_leaf" : [2, 50], 
              "n_estimators" : [100, 250, 500], 
              "max_depth" : [3,5,7], 
              "learning_rate" : [0.05, 0.1], 
              "max_leaf_nodes" : [None, 8, 32]};

param_grid = {"n_estimators" : [250], 
              "max_depth" : [3], 
              "learning_rate" : [0.05, 0.1], 
              "max_leaf_nodes" : [None, 8, 16, 32]};
              
algo = GradientBoostingClassifier();
grid_search = GridSearchCV(algo, param_grid, n_jobs=20, verbose=1, scoring='roc_auc');
start = time()
grid_search.fit(train_feature_matrix, train_target_vector);

print ("Best Hyperparameters");
# best hyperparameter setting
print(grid_search.best_params_);

print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search.grid_scores_)))
print(grid_search.grid_scores_)

print ("Test Set Metrics");
PrintMetrics(test_target_vector, algo.predict(test_feature_matrix), algo.predict_proba(test_feature_matrix)[:,1]);
print ("Validation Set Metrics");
PrintMetrics(validation_target_vector, algo.predict(validation_feature_matrix), algo.predict_proba(validation_feature_matrix)[:,1]);  





  