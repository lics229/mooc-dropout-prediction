#!/bin/bash

awk -F"," '{print $1}' ../data/complete/log_train.csv | uniq -c > act.count
paste act.count ../complete/truth_train.csv > act_count_label.txt
grep -e "^      1.*,0$" act_count_label.txt > one_act_complete.txt