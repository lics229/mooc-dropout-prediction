__author__ = 'dpeng'
from FeatureVectorGen import readFeatureFromCache
root_dir = '../data/final/v5/'

train_target_vector, train_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'train/normalized.csv');
validation_target_vector, validation_feature_matrix, feature_names = readFeatureFromCache(root_dir + 'validation/normalized.csv');

import random
random.seed(1)
num_records = len(train_feature_matrix);
allo = [random.random() for i in range(0, num_records)];
import numpy as np
def sampleAtPercentage(train_target_vector, train_feature_matrix, allo, percentage):
    num_records = len(train_feature_matrix);
    indexes = [i for i in range(0, num_records) if allo[i] < percentage];
    num_samples = len(indexes);
    new_target = np.zeros((num_samples,));
    new_matrix = np.zeros((num_samples, len(train_feature_matrix[0])));
    for i, j in enumerate(indexes):
        new_target[i] = train_target_vector[j];
        for k in range(0, len(train_feature_matrix[0])):
            new_matrix[i][k] = train_feature_matrix[j][k];
    return (new_target, new_matrix)

from FeatureVectorGen import saveFeatures
from evaluatesvmlight import evaluateSVMLight
pts = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];
# pts = [1.0];
aucs = [];
f1_0s = [];
f1_1s = [];
lr_metrics = [];
rf_metrics = [];
gb_metrics = [];
abc_metrics = [];
arg = "-c 0.001 -m 10000 -v 2 -q 50 -n 20";
from transformsvmlight import transformSVMLightAll
import os
with open('../data/final/learning-curve/validation/svm-lc.dat', "w") as f:
    f.write("#Linear SVM learning curve\n");
    for percentage in pts:
        print("Training %f" % percentage)
        (sample_target, sample_matrix) = sampleAtPercentage(train_target_vector, train_feature_matrix, allo, percentage);
        csv_file = '../data/final/learning-curve/svm/train-%f.csv' % percentage;
        data_file = '../data/final/learning-curve/svm/train-%f.dat' % percentage;
        model_file = '../data/final/learning-curve/svm/model-%f.dat' % percentage;
        log_file = '../data/final/learning-curve/svm/log-%f.dat' % percentage;
        prediction_file = '../data/final/learning-curve/svm/prediction-%f.dat' % percentage;
        print("Size: %d" % len(sample_target))
        saveFeatures(sample_target, sample_matrix, feature_names, csv_file);
        transformSVMLightAll(csv_file, data_file);
        os.system('../svmlight/svm_learn %s %s %s > %s' % (arg, data_file, model_file, log_file));
        os.system('../svmlight/svm_classify %s %s %s >> %s' % ('../data/final/v5/svmlight/validation_svm_light.dat', model_file, prediction_file, log_file));
        auc = evaluateSVMLight('../data/final/v5/svmlight/validation_svm_light.dat', prediction_file);
        print("AUC: %f" % auc)
        f.write("%f %f\n" % (percentage, auc))
